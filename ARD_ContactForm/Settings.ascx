﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Settings.ascx.cs" Inherits="ARD.Modules.ARD_ContactForm.Settings" %>


<%@ Register TagName="label" TagPrefix="dnn" Src="~/controls/labelcontrol.ascx" %>

	<h2 id="dnnSitePanel-BasicSettings" class="dnnFormSectionHead">General Settings</h2>
	<fieldset>

        <div class="dnnFormItem">
            <dnn:Label ID="lblAdminEmail" Text = "Admin email address(es)" runat="server" />  
            <asp:TextBox ID="txtAdminEmail" runat="server" />
        </div>
        <div class="dnnFormItem">
            <dnn:Label ID="lblBcc" Text = "Bcc(es)" runat="server" />
            <asp:TextBox ID="txtBcc" runat="server" />
        </div>
        <div class="dnnFormItem">
            <dnn:Label ID="lblSenderEmail" Text = "Sender email address" runat="server" />  
            <asp:TextBox ID="txtSenderEmail" runat="server" />
        </div>
        <div class="dnnFormItem">
            <dnn:Label ID="lblSubject" Text = "Email subject" runat="server" />  
            <asp:TextBox ID="txtSubject" runat="server" />
        </div>
        <div class="dnnFormItem">
            <dnn:Label ID="lblEmailBody" Text = "Email content" runat="server" />  
            <asp:TextBox ID="txtBody" runat="server"  TextMode="MultiLine" Rows="10"/>
        </div>
         <div class="dnnFormItem">
            <dnn:Label ID="lblSuccessMsg" Text = "Message when the email was successfully sent" runat="server" />  
            <asp:TextBox ID="txtSuccessMsg" TextMode="MultiLine" Rows="3" runat="server" />
        </div>

         <div class="dnnFormItem">
            <dnn:Label ID="lblSendEmail" Text = "" runat="server" />  
            <asp:CheckBox ID="chkSendEmail" runat="server" Text="Turn on sending email to admin?" /><br />
        </div>

        <div class="text-center"><h2 class="dnnFormSectionHead">Form localization</h2></div>

        
        <div class="dnnFormItem">
            <dnn:Label ID="lblFullName" Text = "Full name" runat="server" />  
            <asp:TextBox ID="txtFullName" runat="server" />
        </div>
        
        <div class="dnnFormItem">
            <dnn:Label ID="lblFullNameErrMsg" Text = "Full name error msg" runat="server" />  
            <asp:TextBox ID="txtFullNameErrMsg" runat="server" />
        </div>
        
        <div class="dnnFormItem">
            <dnn:Label ID="lblCompanyName" Text = "Company name" runat="server" />  
            <asp:TextBox ID="txtCompanyName" runat="server" />
        </div>
        
        <div class="dnnFormItem">
            <dnn:Label ID="lblCompanyNameErrMsg" Text = "Company name error msg" runat="server" />  
            <asp:TextBox ID="txtCompanyNameErrMsg" runat="server" />
        </div>
        
        <div class="dnnFormItem">
            <dnn:Label ID="lblEmail" Text = "Email" runat="server" />  
            <asp:TextBox ID="txtEmail" runat="server" />
        </div>
        
        <div class="dnnFormItem">
            <dnn:Label ID="lblEmailExistsErrMsg" Text = "Email existence error msg" runat="server" />  
            <asp:TextBox ID="txtEmailExistsErrMsg" runat="server" />
        </div>
        <div class="dnnFormItem">
            <dnn:Label ID="lblEmailValidateErrMsg" Text = "Email validation error msg" runat="server" />  
            <asp:TextBox ID="txtEmailValidateErrMsg" runat="server" />
        </div>
        
        <div class="dnnFormItem">
            <dnn:Label ID="lblPhone" Text = "Phone" runat="server" />  
            <asp:TextBox ID="txtPhone" runat="server" />
        </div>
        
        <div class="dnnFormItem">
            <dnn:Label ID="lblPhoneExistsErrMsg" Text = "Phone existence error msg" runat="server" />  
            <asp:TextBox ID="txtPhoneExistsErrMsg" runat="server" />
        </div>
        <div class="dnnFormItem">
            <dnn:Label ID="lblPhoneValidateErrMsg" Text = "Phone validation error msg" runat="server" />  
            <asp:TextBox ID="txtPhoneValidateErrMsg" runat="server" />
        </div>

        
        <div class="dnnFormItem">
            <dnn:Label ID="lblMessage" Text = "Message" runat="server" />  
            <asp:TextBox ID="txtMessage" runat="server" />
        </div>

        
    </fieldset>


