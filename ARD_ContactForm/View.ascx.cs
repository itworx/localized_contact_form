﻿/*
' Copyright (c) 2017  ARD.com.au
'  All rights reserved.
' 
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
' TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
' THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
' CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
' DEALINGS IN THE SOFTWARE.
' 
*/

using System;
using System.Web.UI.WebControls;
using ARD.Modules.ARD_ContactForm.Components;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Localization;
using DotNetNuke.UI.Utilities;

namespace ARD.Modules.ARD_ContactForm
{
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// The View class displays the content
    /// 
    /// Typically your view control would be used to display content or functionality in your module.
    /// 
    /// View may be the only control you have in your project depending on the complexity of your module
    /// 
    /// Because the control inherits from ARD_ContactFormModuleBase you have access to any custom properties
    /// defined there, as well as properties from DNN such as PortalId, ModuleId, TabId, UserId and many more.
    /// 
    /// </summary>
    /// -----------------------------------------------------------------------------
    public partial class View : ARD_ContactFormModuleBase, IActionable
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {


                hidModID.Value = this.ModuleId.ToString();
                if (Settings.Contains("ARDForm_FullNameLabel"))
                    txtName.Attributes.Add("placeholder", Settings["ARDForm_FullNameLabel"].ToString());
                if (Settings.Contains("ARDForm_FullNameLabelErrorMessage"))
                    rfvName.ErrorMessage = Settings["ARDForm_FullNameLabelErrorMessage"].ToString();

                if (Settings.Contains("ARDForm_CompanyNameLabel"))
                    txtCompany.Attributes.Add("placeholder", Settings["ARDForm_CompanyNameLabel"].ToString());
                if (Settings.Contains("ARDForm_CompanyNameLabelErrorMessage"))
                    rfvCompany.ErrorMessage = Settings["ARDForm_CompanyNameLabelErrorMessage"].ToString();

                if (Settings.Contains("ARDForm_EmailLabel"))
                    txtEmail.Attributes.Add("placeholder", Settings["ARDForm_EmailLabel"].ToString());
                if (Settings.Contains("ARDForm_EmailExistsLabelErrorMessage"))
                    rfvEmail.ErrorMessage = Settings["ARDForm_EmailExistsLabelErrorMessage"].ToString();
                if (Settings.Contains("ARDForm_EmailValidateLabelErrorMessage"))
                    revEmail.ErrorMessage = Settings["ARDForm_EmailValidateLabelErrorMessage"].ToString();

                if (Settings.Contains("ARDForm_PhoneLabel"))
                    txtPhone.Attributes.Add("placeholder", Settings["ARDForm_PhoneLabel"].ToString());
                if (Settings.Contains("ARDForm_PhoneExistsLabelErrorMessage"))
                    rfvPhone.ErrorMessage = Settings["ARDForm_PhoneExistsLabelErrorMessage"].ToString();
                if (Settings.Contains("ARDForm_PhoneValidateLabelErrorMessage"))
                    revPhoneConfirm.ErrorMessage = Settings["ARDForm_PhoneValidateLabelErrorMessage"].ToString();

                if (Settings.Contains("ARDForm_MessageLabel"))
                    txtMessage.Attributes.Add("placeholder", Settings["ARDForm_MessageLabel"].ToString());
            }
            catch (Exception exc) //Module failed to load
            {
                //Exceptions.ProcessModuleLoadException(this, exc);
                throw exc;
            }
        }

        public ModuleActionCollection ModuleActions
        {
            get
            {
                var actions = new ModuleActionCollection
                    {
                        {
                            GetNextActionID(), Localization.GetString("EditModule", LocalResourceFile), "", "", "",
                            EditUrl(), false, SecurityAccessLevel.Edit, true, false
                        }
                    };
                return actions;
            }
        }
    }
}