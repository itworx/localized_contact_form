﻿/*
' Copyright (c) 2017  ARD.com.au
'  All rights reserved.
' 
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
' TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
' THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
' CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
' DEALINGS IN THE SOFTWARE.
' 
*/

using System;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Exceptions;

namespace ARD.Modules.ARD_ContactForm
{
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// The Settings class manages Module Settings
    /// 
    /// Typically your settings control would be used to manage settings for your module.
    /// There are two types of settings, ModuleSettings, and TabModuleSettings.
    /// 
    /// ModuleSettings apply to all "copies" of a module on a site, no matter which page the module is on. 
    /// 
    /// TabModuleSettings apply only to the current module on the current page, if you copy that module to
    /// another page the settings are not transferred.
    /// 
    /// If you happen to save both TabModuleSettings and ModuleSettings, TabModuleSettings overrides ModuleSettings.
    /// 
    /// Below we have some examples of how to access these settings but you will need to uncomment to use.
    /// 
    /// Because the control inherits from ARD_ContactFormSettingsBase you have access to any custom properties
    /// defined there, as well as properties from DNN such as PortalId, ModuleId, TabId, UserId and many more.
    /// </summary>
    /// -----------------------------------------------------------------------------
    public partial class Settings : ARD_ContactFormModuleSettingsBase
    {
        #region Base Method Implementations

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// LoadSettings loads the settings from the Database and displays them
        /// </summary>
        /// -----------------------------------------------------------------------------
        public override void LoadSettings()
        {
            try
            {
                if (Page.IsPostBack == false)
                {

                    if (Settings.Contains("ARDForm_AdminEmailsForClientContact"))
                        txtAdminEmail.Text = Settings["ARDForm_AdminEmailsForClientContact"].ToString();

                    if (Settings.Contains("ARDForm_BccEmailsForClientContact"))
                        txtBcc.Text = Settings["ARDForm_BccEmailsForClientContact"].ToString();

                    if (Settings.Contains("ARDForm_SenderEmail"))
                        txtSenderEmail.Text = Settings["ARDForm_SenderEmail"].ToString();

                    if (Settings.Contains("ARDForm_EmailSubject"))
                        txtSubject.Text = Settings["ARDForm_EmailSubject"].ToString();

                    if (Settings.Contains("ARDForm_EmailBody"))
                        txtBody.Text = Settings["ARDForm_EmailBody"].ToString();

                    if (Settings.Contains("ARDForm_SuccessMsg"))
                        txtSuccessMsg.Text = Settings["ARDForm_SuccessMsg"].ToString();


                    if (Settings.Contains("ARDForm_SendEmailToAdminsEnabled"))
                        chkSendEmail.Checked = GetBoolValueFromSettings("ARDForm_SendEmailToAdminsEnabled");


                    if (Settings.Contains("ARDForm_FullNameLabel"))
                        txtFullName.Text = Settings["ARDForm_FullNameLabel"].ToString();
                    if (Settings.Contains("ARDForm_FullNameLabelErrorMessage"))
                        txtFullNameErrMsg.Text = Settings["ARDForm_FullNameLabelErrorMessage"].ToString();


                    if (Settings.Contains("ARDForm_CompanyNameLabel"))
                        txtCompanyName.Text = Settings["ARDForm_CompanyNameLabel"].ToString();
                    if (Settings.Contains("ARDForm_CompanyNameLabelErrorMessage"))
                        txtCompanyNameErrMsg.Text = Settings["ARDForm_CompanyNameLabelErrorMessage"].ToString();


                    if (Settings.Contains("ARDForm_EmailLabel"))
                        txtEmail.Text = Settings["ARDForm_EmailLabel"].ToString();
                    if (Settings.Contains("ARDForm_EmailExistsLabelErrorMessage"))
                        txtEmailExistsErrMsg.Text = Settings["ARDForm_EmailExistsLabelErrorMessage"].ToString();
                    if (Settings.Contains("ARDForm_EmailValidateLabelErrorMessage"))
                        txtEmailValidateErrMsg.Text = Settings["ARDForm_EmailValidateLabelErrorMessage"].ToString();


                    if (Settings.Contains("ARDForm_PhoneLabel"))
                        txtPhone.Text = Settings["ARDForm_PhoneLabel"].ToString();
                    if (Settings.Contains("ARDForm_PhoneExistsLabelErrorMessage"))
                        txtPhoneExistsErrMsg.Text = Settings["ARDForm_PhoneExistsLabelErrorMessage"].ToString();
                    if (Settings.Contains("ARDForm_PhoneValidateLabelErrorMessage"))
                        txtPhoneValidateErrMsg.Text = Settings["ARDForm_PhoneValidateLabelErrorMessage"].ToString();


                    if (Settings.Contains("ARDForm_MessageLabel"))
                        txtMessage.Text = Settings["ARDForm_MessageLabel"].ToString();

                }
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }
        }

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// UpdateSettings saves the modified settings to the Database
        /// </summary>
        /// -----------------------------------------------------------------------------
        public override void UpdateSettings()
        {
            try
            {
                var modules = new ModuleController();

                modules.UpdateModuleSetting(ModuleId, "ARDForm_AdminEmailsForClientContact", txtAdminEmail.Text);
                modules.UpdateModuleSetting(ModuleId, "ARDForm_BccEmailsForClientContact", txtBcc.Text);
                modules.UpdateModuleSetting(ModuleId, "ARDForm_SenderEmail", txtSenderEmail.Text);
                modules.UpdateModuleSetting(ModuleId, "ARDForm_EmailSubject", txtSubject.Text);
                modules.UpdateModuleSetting(ModuleId, "ARDForm_EmailBody", txtBody.Text);
                modules.UpdateModuleSetting(ModuleId, "ARDForm_SuccessMsg", txtSuccessMsg.Text);
                modules.UpdateModuleSetting(ModuleId, "ARDForm_SendEmailToAdminsEnabled", chkSendEmail.Checked.ToString());


                modules.UpdateModuleSetting(ModuleId, "ARDForm_FullNameLabel", txtFullName.Text);
                modules.UpdateModuleSetting(ModuleId, "ARDForm_FullNameLabelErrorMessage", txtFullNameErrMsg.Text);

                modules.UpdateModuleSetting(ModuleId, "ARDForm_CompanyNameLabel", txtCompanyName.Text);
                modules.UpdateModuleSetting(ModuleId, "ARDForm_CompanyNameLabelErrorMessage", txtCompanyNameErrMsg.Text);

                modules.UpdateModuleSetting(ModuleId, "ARDForm_EmailLabel", txtEmail.Text);
                modules.UpdateModuleSetting(ModuleId, "ARDForm_EmailExistsLabelErrorMessage", txtEmailExistsErrMsg.Text);
                modules.UpdateModuleSetting(ModuleId, "ARDForm_EmailValidateLabelErrorMessage", txtEmailValidateErrMsg.Text);

                modules.UpdateModuleSetting(ModuleId, "ARDForm_PhoneLabel", txtPhone.Text);
                modules.UpdateModuleSetting(ModuleId, "ARDForm_PhoneExistsLabelErrorMessage", txtPhoneExistsErrMsg.Text);
                modules.UpdateModuleSetting(ModuleId, "ARDForm_PhoneValidateLabelErrorMessage", txtPhoneValidateErrMsg.Text);

                modules.UpdateModuleSetting(ModuleId, "ARDForm_MessageLabel", txtMessage.Text);

            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }
        }

        private bool GetBoolValueFromSettings(string key)
        {
            bool retVal = false;

            try
            {
                if (Settings.Contains(key))
                    retVal = Convert.ToBoolean(Settings[key]);
            }
            catch (Exception)
            {
            }

            return retVal;
        }

        #endregion
    }
}