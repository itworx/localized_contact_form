﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View.ascx.cs" Inherits="ARD.Modules.ARD_ContactForm.View" %>
<%@ Register TagPrefix="dnn" TagName="TITLE" Src="~/Admin/Containers/Title.ascx" %>

<i aria-hidden="true" class="fa fa-envelope"></i>
<h4><dnn:TITLE runat="server" id="dnnTITLE" /></h4>
<div id="ContactForm">
    <div class="form-group raw">
        <div class="col-md-12 col-xs-12">
            <asp:TextBox ID="txtName" CssClass="form-control" runat="server" placeholder="Full name" MaxLength="30"></asp:TextBox> 
        </div>

        <div class="col-md-12 col-xs-12 validator-container">
            <asp:RequiredFieldValidator  ID="rfvName" CssClass="validator" runat="server" ControlToValidate="txtName"  Display="Dynamic" ErrorMessage="Please enter your full name"></asp:RequiredFieldValidator>             
        </div>
    </div>

    <div class="clearfix"></div>
    
    <div class="form-group raw">
        <div class="col-md-12 col-xs-12">
            <asp:TextBox ID="txtCompany" CssClass="form-control" runat="server" placeholder="Company name" MaxLength="30"></asp:TextBox> 
        </div>

        <div class="col-md-12 col-xs-12 validator-container">
            <asp:RequiredFieldValidator  ID="rfvCompany" CssClass="validator" runat="server" ControlToValidate="txtCompany"  Display="Dynamic" ErrorMessage="Please enter your company name"></asp:RequiredFieldValidator>             
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="form-group raw">
        <div class="col-md-12 col-xs-12">
            <asp:TextBox ID="txtEmail" CssClass="form-control email" runat="server" placeholder="Email" MaxLength="30"></asp:TextBox> 
        </div>

        <div class="col-md-12 col-xs-12 validator-container">
            <asp:RequiredFieldValidator  ID="rfvEmail" CssClass="validator" runat="server" ControlToValidate="txtEmail"  Display="Dynamic" ErrorMessage="Please enter your email"></asp:RequiredFieldValidator>            
            <asp:RegularExpressionValidator ID="revEmail" CssClass="validator" runat="server" 
                        ErrorMessage="Please enter a valid email" ControlToValidate="txtEmail"
                        ValidationExpression="\s*\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*\s*" Display="Dynamic"> 
            </asp:RegularExpressionValidator> 
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="form-group raw">
        <div class="col-md-12 col-xs-12">
            <asp:TextBox ID="txtPhone" CssClass="form-control" runat="server" placeholder="Phone number" MaxLength="16"></asp:TextBox>
        </div>

        <div class="col-md-12 col-xs-12 validator-container">
            <asp:RequiredFieldValidator  ID="rfvPhone" CssClass="validator" runat="server" ControlToValidate="txtPhone"  Display="Dynamic" ErrorMessage="Please enter your phone number"></asp:RequiredFieldValidator> 
            <asp:RegularExpressionValidator ID="revPhoneConfirm" CssClass="validator" runat="server" 
                        ControlToValidate="txtPhone" ErrorMessage="Please enter a valid phone number" 
                        ValidationExpression="^(?=(?:\D*\d){10,15}\D*$)\+?[0-9]{1,3}[\s-]?(?:\(0?[0-9]{1,5}\)|[0-9]{1,5})[-\s]?[0-9][\d\s-]{5,7}\s?(?:x[\d-]{0,4}|[0-9]{1,5})?$" Display="Dynamic">
            </asp:RegularExpressionValidator>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="form-group raw">
        <div class="col-md-12 col-xs-12">
            <asp:TextBox ID="txtMessage" CssClass="form-control" runat="server" Rows="10" placeholder="Message (Optional)" MaxLength="4000" TextMode="MultiLine"></asp:TextBox>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="form-group raw">
        <div class="col-md-12 col-xs-12">
             <input id="send" type="button" value="" />
         </div>
    </div>

    <div class="clearfix"></div>

    <div id="loadingDiv">
        <div id="loadingDivImage">
    </div>

    <asp:HiddenField id="hidModID" runat="server"></asp:HiddenField>
</div>
    <div>
        
        <div id="registrationSuccessMessage"><asp:Label ID="lblMsg" runat="server"></asp:Label></div>
    </div>

<script type="text/javascript">

    function message(state) {
        //if (state == 'success') { $('#registrationSuccessMessage span').removeClass().addClass('alert alert-success'); }
        //if (state == 'fail') { $('#registrationSuccessMessage span').removeClass().addClass('alert alert-danger'); }
        if (state == 'clear') { { $('#registrationSuccessMessage span').removeClass(); } }
    }

    var selectedFiles;
    var $loading = $('#loadingDiv').hide();

    $(document).ready(function () {

        var $txtName = $("#<%=txtName.ClientID%>");

        var $txtCompany = $("#<%=txtCompany.ClientID%>");

        var $txtEmail = $("#<%=txtEmail.ClientID%>");

        var $txtPhone = $("#<%=txtPhone.ClientID%>");

        var $txtMessage = $("#<%=txtMessage.ClientID%>");

        var $lblMsg = $('#<%=lblMsg.ClientID%>');

        var $lblModuleID = $('#<%=hidModID.ClientID%>');

        $txtName
            .add($txtCompany)
            .add($txtEmail)
            .add($txtPhone)
            .add($txtMessage)
            .on('focus', function () {
                if ($lblMsg.html().length > 0) {
                    $lblMsg.html('');
                }
            });


        $("#send").click(function () {

            if (Page_ClientValidate()) {
                var data = new FormData();

                data.append("Name", $txtName.val());

                data.append("Company", $txtCompany.val());

                data.append("Email", $txtEmail.val());

                data.append("Phone", $txtPhone.val());

                data.append("Message", $txtMessage.val());

                data.append("ModuleID", $lblModuleID.val());

                $.ajax({
                    type: "POST",
                    url: "/DesktopModules/ARD_ContactForm/ARDContactFormHandler.ashx",
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (data) {


                        if (data == null || data == undefined) {
                            $lblMsg.html('<font>Nothing returned from the server.</font>');
                            message('fail');
                            return false;
                        }

                        if (data.successMsg == 'success' || data.responseCode == "0") {
                            $lblMsg.html(data.msgToBeShownToUser);
                            message('success');
                        }
                        else if (data.successMsg == 'success' || data.responseCode == "100") {
                            $lblMsg.html('<font>Failed to send email.</font>');
                            message('fail');
                        }
                        else {
                            $lblMsg.html(data.errorMsg);
                            message('fail');
                        }

                        $txtName.val('');

                        $txtCompany.val('');

                        $txtEmail.val('');

                        $txtPhone.val('');

                        $txtMessage.val('');
                    },
                    error: function () {
                        $lblMsg.html('<font>There was error uploading files!</font>');
                        message('fail');
                    }
                });
            }
        });
    });

    $(document)
        .ajaxStart(function () {
            $loading.show();
        })
        .ajaxStop(function () {
            $loading.hide();
        });
</script>
