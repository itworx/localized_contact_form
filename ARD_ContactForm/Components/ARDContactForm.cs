﻿using System;
using System.Web.Caching;
using DotNetNuke.Common.Utilities;
using DotNetNuke.ComponentModel.DataAnnotations;
using DotNetNuke.Entities.Content;
namespace ARD.Modules.ARD_ContactForm.Components
{
    [TableName("ARD_ContactForms")]
    [PrimaryKey("ID", AutoIncrement = true)]
    public class ARDContactForm
    {
        public ARDContactForm()
        {
            CreatedOnDate = DateTime.Now;
        }

        public int ID { get; set; }

        public string Name { get; set; }

        public string Company { get; set; }

        public string Email { get; set; }
        
        public string Phone { get; set; }
        
        public string Message { get; set; }
        
        public string EmailSentToAdmin { get; set; }

        public DateTime? EmailSentOnDate { get; set; }

        public string Description { get; set; }

        public DateTime? CreatedOnDate { get; set; }

        public int CreatedByUserId { get; set; }

        public DateTime? LastModifiedOnDate { get; set; }

        public int LastModifiedByUserId { get; set; }

    }
}