﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


using DotNetNuke.Data;


namespace ARD.Modules.ARD_ContactForm.Components
{
    public class ARDContactFormController
    {
        public ARDContactFormController()
        {

        }

        #region CreateARDContactForm: It inserts a new record to the table named ARDContactForm in DB
        /// <summary>
        /// It inserts a new record to the table named ARDContactForm in DB
        /// </summary>
        /// <param name="rego"></param>
        public void CreateARDContactForm(ARDContactForm rego)
        {
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<ARDContactForm>();
                rep.Insert(rego);
            }
        }
        #endregion

        #region UpdateARDContactForm: It updates a record to the table named ARDContactForm in DB
        /// <summary>
        /// It updates a record to the table named ARDContactForm in DB
        /// </summary>
        /// <param name="rego"></param>
        public void UpdateARDContactForm(ARDContactForm rego)
        {
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<ARDContactForm>();
                rep.Update(rego);
            }
        }
        #endregion

        #region GetLatestRegoByEmail: It retrieves a user by email
        /// <summary>
        /// It retrieves a user by email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public ARDContactForm GetLatestRegoByEmail(string email)
        {
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<ARDContactForm>();
                var users = rep.Find(string.Format(" WHERE EMAIL = '{0}'", email.Replace("@", "@@")));

                var retUser = new ARDContactForm();
                if (users != null && users.Count() > 0)
                {
                    retUser = users.OrderByDescending(p => p.CreatedOnDate).FirstOrDefault();
                }
                return retUser;
            }
        }
        #endregion
    }
}