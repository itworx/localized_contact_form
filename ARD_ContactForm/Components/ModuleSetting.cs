﻿using System;
using System.Web.Caching;
using DotNetNuke.Common.Utilities;
using DotNetNuke.ComponentModel.DataAnnotations;
using DotNetNuke.Entities.Content;

namespace ARD.Modules.ARD_ContactForm.Components
{
    [TableName("ModuleSettings")]

    public class ModuleSetting
    {
        public ModuleSetting()
        {
        }

        public int ModuleID { get; set; }

        public string SettingName { get; set; }

        public string SettingValue { get; set; }

        public int? CreatedByUserID { get; set; }

        public DateTime? CreatedOnDate { get; set; }

        public int LastModifiedByUserID { get; set; }

        public DateTime? LastModifiedOnDate { get; set; }
    }
}