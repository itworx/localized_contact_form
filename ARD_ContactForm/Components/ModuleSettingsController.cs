﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


using DotNetNuke.Data;
using System.Collections;

namespace ARD.Modules.ARD_ContactForm.Components
{
    public class ModuleSettingsController
    {
        public ModuleSettingsController()
        {

        }

        public Hashtable GetModuleSettingsFromDB()
        {
            Hashtable ModuleSettings = new Hashtable();

            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<ModuleSetting>();
                var settings = rep.Find(" WHERE SettingName like 'ARDForm%'").OrderBy(p => p.SettingName);

                foreach (var setting in settings)
                {
                    if (!ModuleSettings.ContainsKey(setting.SettingName))
                        ModuleSettings.Add(setting.SettingName, setting.SettingValue);
                }

            }

            return ModuleSettings;
        }

        public Hashtable GetModuleSettingsFromDB(int moduleID)
        {
            Hashtable ModuleSettings = new Hashtable();

            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<ModuleSetting>();
                var settings = rep.Find(" WHERE ModuleID = " + moduleID.ToString() + " AND SettingName like 'ARDForm%'").OrderBy(p => p.SettingName);

                foreach (var setting in settings)
                {
                    if (!ModuleSettings.ContainsKey(setting.SettingName))
                        ModuleSettings.Add(setting.SettingName, setting.SettingValue);
                }

            }

            return ModuleSettings;
        }
    }
}