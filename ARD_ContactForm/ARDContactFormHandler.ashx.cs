﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ARD.Modules.ARD_ContactForm.Components;
using DotNetNuke.Services.Mail;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Localization;
using System.IO;
using System.Collections;
using System.Text;

namespace ARD.Modules.ARD_ContactForm
{
    /// <summary>
    /// Summary description for ARDContactFormHandler
    /// </summary>
    public class ARDContactFormHandler : ARD_ContactFormModuleBase, IHttpHandler
    {

        Hashtable ModuleSettings = new Hashtable();
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                var rego = new ARDContactForm();
                rego.Email = GetValue(context, "Email");
                rego.Name = GetValue(context, "Name");
                rego.Company = GetValue(context, "Company");

                if (string.IsNullOrEmpty(rego.Email) ||
                    string.IsNullOrEmpty(rego.Name))
                    new Exception("Email or Name cannot be null");
                
                rego.Phone = GetValue(context, "Phone");
                rego.Message = GetValue(context, "Message");

                rego.Description = string.Empty;
                rego.CreatedByUserId = -1;
                rego.LastModifiedOnDate = DateTime.Now;
                rego.LastModifiedByUserId = -1;
                
                var strModuleID = GetValue(context, "ModuleID");
                int iModuleID;
                if (int.TryParse(strModuleID, out iModuleID) && iModuleID > 0)
                    ModuleSettings = new ModuleSettingsController().GetModuleSettingsFromDB(iModuleID);
                else
                    ModuleSettings = new ModuleSettingsController().GetModuleSettingsFromDB();

                
                if (!string.IsNullOrEmpty(rego.Name) &&
                    !string.IsNullOrEmpty(rego.Email) &&
                    !string.IsNullOrEmpty(rego.Company))
                {
                    new ARDContactFormController().CreateARDContactForm(rego);
                }

                var bSuccess = SendEmail(context, rego);
                var responseCode = bSuccess ? "0" : "100";
                var msgToBeShownToUser = (ModuleSettings["ARDForm_SuccessMsg"] != null) ? ModuleSettings["ARDForm_SuccessMsg"].ToString() : "Thank you for your registration!";

                //////////////////////////////////
                //msgToBeShownToUser = string.Empty;
                //////////////////////////////////

                ResponseToClientInJSON(context, "success", responseCode, "", msgToBeShownToUser);

            }
            catch (Exception ex)
            {
                ResponseToClientInJSON(context, "error", "-1", ex.Message, "Sorry, there is a temporary system error!");
            }
        }
        
        private string GetValue(HttpContext context, string key)
        {
            string retVal = string.Empty;

            if (context.Request[key] != null)
                retVal = context.Request[key];

            return retVal;
        }

        #region SendEmail: It sends the message to admin(s)
        /// <summary>
        /// It sends the message to admin(s)
        /// </summary>
        /// <param name="context"></param>
        /// <param name="rego"></param>
        protected bool SendEmail(HttpContext context, ARDContactForm rego)
        {
            bool bRetVal = true;
            try
            {
                if (IsSendEmailToAdminsEnabled && !string.IsNullOrEmpty(AdminEmailsForClientContact))
                {
                    var ARDContactFormController = new ARDContactFormController();
                    var lstSenderEmail = GetSettingInfoFromDB("ARDForm_SenderEmail");
                    var senderEmail = "no-reply@itworx.com.au";
                    if (lstSenderEmail.Count > 0)
                        senderEmail = lstSenderEmail[0];
                    var subject = (ModuleSettings["ARDForm_EmailSubject"] != null) ? ModuleSettings["ARDForm_EmailSubject"].ToString() : "Enquiry from client " + DateTime.Now.ToString();
                    var body = string.Empty;
                    if (ModuleSettings["ARDForm_EmailBody"] != null && !string.IsNullOrEmpty(ModuleSettings["ARDForm_EmailBody"].ToString()))
                        body = ModuleSettings["ARDForm_EmailBody"].ToString();
                    if (string.IsNullOrEmpty(body))
                    {
                        StringBuilder sb = new StringBuilder();
                        string url = HttpContext.Current.Request.Url.Host;

                        sb.Append("<p>Here is a new contact details from " + url + "</p>");
                        
                        sb.Append("<div>");
                        sb.Append("     <ul>");

                        sb.Append("         <li>");
                        sb.Append("Full name: " + rego.Name );
                        sb.Append("         </li>");

                        sb.Append("         <li>");
                        sb.Append("Company name: " + rego.Company);
                        sb.Append("         </li>");

                        sb.Append("         <li>");
                        sb.Append("Email: " + rego.Email);
                        sb.Append("         </li>");

                        sb.Append("         <li>");
                        sb.Append("Phone number: " + rego.Phone);
                        sb.Append("         </li>");
                        
                        if (!string.IsNullOrEmpty(rego.Message))
                        {
                            sb.Append("         <li>");
                            sb.Append("Message: " + rego.Message);
                            sb.Append("         </li>");
                        }

                        sb.Append("     </ul>");
                        sb.Append("<p>Please follow up this enquiry as soon as possible.</p>");
                        sb.Append("<p>Thank you.</p>");
                        sb.Append("</div>");


                        body += sb.ToString();
                    }
                    else
                    {
                        body = body.Replace(@"[[name]]", rego.Name);
                        body = body.Replace(@"[[company]]", rego.Company);
                        body = body.Replace(@"[[email]]", rego.Email);
                        body = body.Replace(@"[[phone]]", rego.Phone);
                        body = body.Replace(@"[[message]]", rego.Message);
                    }
                                 
                    var bccEmailsForClientContact = BccEmailsForClientContact;

                    var userFromDB = ARDContactFormController.GetLatestRegoByEmail(rego.Email);
                    try
                    {
                        Mail.SendMail(senderEmail, AdminEmailsForClientContact, bccEmailsForClientContact, subject, body, string.Empty, "html", string.Empty, string.Empty, string.Empty, string.Empty);
                        if (userFromDB != null && userFromDB.ID > 0)
                        {
                            userFromDB.EmailSentToAdmin = AdminEmailsForClientContact;
                            userFromDB.EmailSentOnDate = DateTime.Now;
                            ARDContactFormController.UpdateARDContactForm(userFromDB);
                        }

                    }
                    catch (Exception ex)
                    {
                        bRetVal = false;
                        if (userFromDB != null && userFromDB.ID > 0)
                        {
                            userFromDB.Description = (string.IsNullOrEmpty(userFromDB.Description)) ? "" : userFromDB.Description;
                            userFromDB.Description += ex.Message;
                            ARDContactFormController.UpdateARDContactForm(userFromDB);
                        }
                    }

                }
                else
                {
                    throw new Exception("Page is not valid");
                }
            }
            catch (Exception ex)
            {
                bRetVal = false;
            }

            return bRetVal;
        }
        #endregion


        private bool GetBoolValueFromSettings(string key)
        {
            bool retVal = false;

            try
            {
                if (ModuleSettings.Contains(key))
                    retVal = Convert.ToBoolean(ModuleSettings[key]);
            }
            catch (Exception)
            {
            }

            return retVal;
        }

        private void ResponseToClientInJSON(HttpContext context, string successMsg, string responseCode, string errorMsg, string msgToBeShownToUser)
        {
            string strRetVal = string.Empty;
            successMsg = (string.IsNullOrEmpty(successMsg)) ? "success" : successMsg.Replace("\"", "").Replace("'", "");
            responseCode = (string.IsNullOrEmpty(responseCode)) ? "0" : responseCode;
            errorMsg = (string.IsNullOrEmpty(errorMsg)) ? "" : errorMsg.Replace("\"", "").Replace("'", "");

            string strJSON = "{\"successMsg\":" + "\"" + successMsg + "\", \"responseCode\":" + "\"" + responseCode + "\", \"msgToBeShownToUser\":" + "\"" + msgToBeShownToUser + "\", \"errorMsg\":" + "\"" + errorMsg + "\"}";

            context.Response.Clear();
            context.Response.ContentType = "application/json; charset=utf-8";
            context.Response.Write(strJSON);
            context.Response.Flush();
            HttpContext.Current.ApplicationInstance.CompleteRequest();


        }

        #region GetSettingInfoFromDB: It reads a value to key from DB
        /// <summary>
        /// It reads a value to key from DB
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private List<string> GetSettingInfoFromDB(string key)
        {
            var lstValues = new List<string>();
            if (ModuleSettings.Contains(key))
            {
                var value = ModuleSettings[key].ToString();
                if (!string.IsNullOrWhiteSpace(value))
                {
                    try
                    {
                        var lstEmails = value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (var email in lstEmails)
                        {
                            if (!string.IsNullOrWhiteSpace(email))
                            {
                                lstValues.Add(email.Trim());
                            }
                        }

                    }
                    catch (Exception)
                    {
                    }
                }
            }


            return lstValues;
        }
        #endregion

        #region IsShowCompany: It determines whether or not the form shows the company name field
        /// <summary>
        /// It determines whether or not the form shows the company name field
        /// </summary>
        public bool IsSendEmailToAdminsEnabled
        {
            get
            {
                return Convert.ToBoolean(GetBoolValueFromSettings("ARDForm_SendEmailToAdminsEnabled"));
            }

        }
        #endregion

        #region BccEmailsForClientContact: It returns a list of Bcc email address(es)
        /// <summary>
        /// It returns a list of Bcc email address(es)
        /// </summary>
        public string BccEmailsForClientContact
        {
            get
            {
                return (ModuleSettings["ARDForm_BccEmailsForClientContact"] != null) ? ModuleSettings["ARDForm_BccEmailsForClientContact"].ToString() : string.Empty;
            }

        }
        #endregion

        #region AdminEmailsForClientContact: It returns a list of admin email address(es)
        /// <summary>
        /// It returns a list of admin email address(es)
        /// </summary>
        public string AdminEmailsForClientContact
        {
            get
            { 
                return (ModuleSettings["ARDForm_AdminEmailsForClientContact"] != null) ? ModuleSettings["ARDForm_AdminEmailsForClientContact"].ToString() : string.Empty;
            }

        }
        #endregion

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}